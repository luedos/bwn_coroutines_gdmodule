#include "precompiled_coroutines.hpp"

#include "register_types.h"

#include "bwn_coroutines/coroutineChannel.hpp"
#include "bwn_coroutines/coroutineContext.hpp"
#include "bwn_coroutines/coroutineOperation.hpp"
#include "bwn_coroutines/coroutineResult.hpp"
#include "bwn_coroutines/coroutineManager.hpp"
#include "bwn_coroutines/coroutineSemaphore.hpp"
#include "bwn_coroutines/operations/coroutineWaiterOperation.hpp"
#include "bwn_coroutines/operations/coroutineWaiterResult.hpp"

void initialize_bwn_coroutines_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
		bwn::CoroutineManager::createSingleton();
	}
	else if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE)
	{
		GDREGISTER_CLASS(bwn::CoroutineChannelObject);
		GDREGISTER_CLASS(bwn::CoroutineContext);
		GDREGISTER_CLASS(bwn::CoroutineOperation);
		GDREGISTER_CLASS(bwn::CoroutineResult);
		GDREGISTER_CLASS(bwn::CoroutineManagerUpdate);
		GDREGISTER_CLASS(bwn::CoroutineSemaphore);
		GDREGISTER_CLASS(bwn::CoroutineManagerUpdate);

		GDREGISTER_CLASS(bwn::CoroutineWaiterOperation);
		GDREGISTER_CLASS(bwn::CoroutineWaiterResult);
	}
}

void uninitialize_bwn_coroutines_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
		bwn::CoroutineManager::destroySingleton();
	}
}