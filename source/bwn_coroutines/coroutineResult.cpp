#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/coroutineResult.hpp"

bwn::CoroutineResult::CoroutineResult()
	: m_result(SystemFacility::createSuccess())
#if defined(DEBUG_ENABLED)
	, m_creatorInfo( nullptr )
#endif
{}

bwn::CoroutineResult::CoroutineResult(const SystemResult& _result)
	: m_result(_result)
{}

bwn::CoroutineResult::~CoroutineResult() = default;

void bwn::CoroutineResult::_bind_methods()
{
	// Protected godot interface
	ClassDB::bind_method(D_METHOD("_init_result_success"), &CoroutineResult::initSystemResultSuccess);
	ClassDB::bind_method(D_METHOD("_init_result_generic_error"), &CoroutineResult::initGenericSystemResultError);
	ClassDB::bind_method(D_METHOD("_init_result_error"), &CoroutineResult::initCompleteSystemResultError);

	// Public godot interface
	ClassDB::bind_method(D_METHOD("is_succeeded"), &CoroutineResult::isSucceeded);
	ClassDB::bind_method(D_METHOD("is_failed"), &CoroutineResult::isFailed);
	
	ClassDB::bind_method(D_METHOD("get_error_code"), &CoroutineResult::getSystemResultCode);
	ClassDB::bind_method(D_METHOD("get_native_error_code"), &CoroutineResult::getSystemResultNativeCode);
	ClassDB::bind_method(D_METHOD("get_error_description"), &CoroutineResult::getSystemResultDescription);
}

bool bwn::CoroutineResult::isSucceeded() const
{
	return m_result.success();
}

bool bwn::CoroutineResult::isFailed() const
{
	return !m_result.success();
}

const bwn::SystemResult& bwn::CoroutineResult::getSystemResult() const
{
	return m_result;
}

Ref<bwn::CoroutineResult> bwn::CoroutineResult::createResult(Error _error, const String& _description)
{
	return createResult(SystemFacility::createError(_error, _description));
}

Ref<bwn::CoroutineResult> bwn::CoroutineResult::createResult(const SystemResult& _result)
{
	return bwn::instantiateRef<CoroutineResult>(_result);
}

Ref<bwn::CoroutineResult> bwn::CoroutineResult::createSuccessResult()
{
	return CoroutineResult::createResult(SystemFacility::createSuccess());
}

#if defined(DEBUG_ENABLED)
void bwn::CoroutineResult::setOperationCreatorInfo(std::shared_ptr<debug::CoroutineCreatorInfo> _info)
{
	m_creatorInfo = _info;
}
std::shared_ptr<const bwn::debug::CoroutineCreatorInfo> bwn::CoroutineResult::getOperationCreatorInfo() const
{
	return m_creatorInfo;
}
#endif

void bwn::CoroutineResult::setSystemResult(const SystemResult& _result)
{
	m_result = _result;
}

void bwn::CoroutineResult::initSystemResultSuccess()
{
	m_result = bwn::SystemFacility::createSuccess();
}

void bwn::CoroutineResult::initGenericSystemResultError(Error _godotError, const String& _description)
{
	m_result = bwn::SystemFacility::createError(_godotError, _description);
}

void bwn::CoroutineResult::initCompleteSystemResultError(SystemResult::NativeError _errorCode, const String& _description, int32_t _facilityId)
{
	bwn::BaseFacility*const facility = bwn::BaseFacility::findFacility(_facilityId);
	BWN_ASSERT_WARNING_COND(facility != nullptr, "Can't create system result because, facility with id '{}' not found.", _facilityId);
	if (facility == nullptr)
	{
		m_result = bwn::SystemFacility::createError(Error::FAILED);
	}
	else
	{
		m_result = bwn::SystemResult(_errorCode, _description, *facility);
	}
}

Error bwn::CoroutineResult::getSystemResultCode() const
{
	return m_result.getError();
}

bwn::SystemResult::NativeError bwn::CoroutineResult::getSystemResultNativeCode() const
{
	return m_result.getNativeError();
}

String bwn::CoroutineResult::getSystemResultDescription() const
{
	return m_result.getErrorDescription();
}