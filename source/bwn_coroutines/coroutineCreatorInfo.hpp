#pragma once

#if defined(DEBUG_ENABLED)

#include <core/string/ustring.h>

#include <memory>
#include <vector>

namespace bwn::debug
{
	class CoroutineCreatorInfo : public std::enable_shared_from_this<CoroutineCreatorInfo>
	{

		//
		// Construction and destruction.
		//
	public:
		CoroutineCreatorInfo(const String& _description);
		virtual ~CoroutineCreatorInfo();

		//
		// Public interface.
		//
	public:
		// Sets parent relation which later could be used to create relation stack.
		void setParentCreator(std::shared_ptr<CoroutineCreatorInfo> _relation);
		// Creates relation description.
		virtual String composeDescriptionV() const;
		// Creates creation description stack. Current relation always the last one.
		std::vector<String> composeCreationStack() const;
		// Also creates creation description stack, but also joins it into one string.
		String composeFullDescription(const bwn::string_view& _separator = U"\n") const;

		//
		// Private members.
		//
	private:
		std::shared_ptr<CoroutineCreatorInfo> m_parentCreator;
		String m_description;
	};
} // namespace bwn

#endif