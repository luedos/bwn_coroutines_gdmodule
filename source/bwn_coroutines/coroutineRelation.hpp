#pragma once

#include <cstdint>

namespace bwn
{
	enum ECoroutineRelation : uint32_t
	{
		k_none = 0x0,
		k_parent = 1 << 0,
		k_waiter = 1 << 1
	};
} // namespace bwn