#pragma once

#include "bwn_coroutines/coroutineRelation.hpp"

#include <core/object/ref_counted.h>

#if defined(DEBUG_ENABLED)
	#include "bwn_coroutines/coroutineCreatorInfo.hpp"
	#include <memory>
#endif

namespace bwn
{
	class CoroutineOperation;
	class CoroutineContext;
	class CoroutineSemaphore;
	class CoroutineWaiterOperation;

	class CoroutineChannel
	{
		//
		// Private classes.
		//
	private:
	#if defined(DEBUG_ENABLED)
		class DebugCreatorInfo;
	#endif

		//
		// Public typedefs.
		//
	public:
		using ChannelId = uint32_t; 
		static constexpr ChannelId k_invalidId = ChannelId(-1);

		//
		// Construction and destruction.
		//
	public:
		// Default constructor.
		CoroutineChannel();
		// Default destructor.
		~CoroutineChannel();

		//
		// Protected constructors.
		//
	protected:
		// This is mostly just for the godot wrapper, which could be initialized from existed channel.
		CoroutineChannel(const CoroutineChannel& _other);
		CoroutineChannel& operator=(const CoroutineChannel& _other);

		//
		// Public interface.
		//
	public:
		// Adds operation to the coroutine manager, with id of that channel.
		bool addOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineContext>& _context) const;
		// Adds operation to the coroutine manager, with parent operation and id of that channel.
		bool addOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineOperation>& _relativeOperation,
			ECoroutineRelation _relationMode, 
			const Ref<CoroutineContext>& _context) const;

		// Starts the recording of the operations. Later, completion of the recorded operations could be tracked.
		void startRecording();
		// Ends recording of the operations. Context will report when all of the operations finished executing.
		void finishRecording(const Ref<CoroutineContext>& _context);
		// Cancel recording of any operations in the channel, without starting to track recorded operations.
		void cancelRecording();

		// Aborts all operations under this specific channel.
		void abortOperations() const;
		// Resets default semaphore for the operations.
		void setSemaphore(const Ref<CoroutineSemaphore>& _semaphore);
		// Pushes operation which tracks all operations from this channel.
		void trackCurrentOperations(const Ref<CoroutineContext>& _context) const;

	#if defined(DEBUG_ENABLED)
		void setParentCreatorInfo(std::shared_ptr<debug::CoroutineCreatorInfo> _info);
		std::shared_ptr<const debug::CoroutineCreatorInfo> getCreatorInfo() const;
	#endif

		//
		// Private methods.
		//
	private:
		// Generates new id, so each channel is unique.
		static ChannelId generateNewId();

		//
		// Private members.
		//
	private:
		// Default semaphore for the operations going through that channel.
		Ref<CoroutineSemaphore> m_semaphore;
		// If this operation is valid, then channel is working in state of recording.
		mutable Ref<CoroutineWaiterOperation> m_waiterOperation;
		// Special id used to tie operations to this specific channel in coroutine manager.
		ChannelId m_id = k_invalidId;
	#if defined(DEBUG_ENABLED)
		std::shared_ptr<DebugCreatorInfo> m_creatorInfo;
	#endif
	};

	// This is basically just a helper class, to be used from gdscript.
	class CoroutineChannelObject : public RefCounted, public CoroutineChannel
	{
		GDCLASS(CoroutineChannelObject, RefCounted);

		//
		// Construction and destruction.
		//
	public:
		CoroutineChannelObject();
		// So we can use this wrapper on existing channels.
		CoroutineChannelObject(const CoroutineChannel& _other);
		virtual ~CoroutineChannelObject() override;

		//
		// Godot methods.
		//
	public:
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		// This is mostly just forwarding method from godot interface.
		// Adds operation to the coroutine manager, with id of that channel.
		bool addOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineContext>& _context) const;
		// Adds operation to the coroutine manager, with parent operation and id of that channel.
		bool addChildOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineOperation>& _parent, 
			const Ref<CoroutineContext>& _context) const;
		// Same as addOperation adds operation to the coroutine manager, but also makes waiter operation wait for this operation.
		bool addAndWaitOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineOperation>& _waiterOperation, 
			const Ref<CoroutineContext>& _context) const;
		// Same as addChildOperation adds operation to the coroutine manager as child of parent,
		// but also makes parent wait for this operation.
		bool addAndWaitChildOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineOperation>& _parent, 
			const Ref<CoroutineContext>& _context) const;
		// Aborts all operations under this specific channel.
		void abortOperations() const;
		// Resets default semaphore for the operations.
		void setSemaphore(const Ref<CoroutineSemaphore>& _semaphore);
		// Pushes operation which tracks all operations from this channel.
		void trackCurrentOperations(const Ref<CoroutineContext>& _context) const;
	};
} // namespace bwn