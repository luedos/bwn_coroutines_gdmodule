#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/coroutineOperation.hpp"
#include "bwn_coroutines/coroutineResult.hpp"
#include "bwn_coroutines/coroutineManager.hpp"
#include "bwn_coroutines/coroutineSemaphore.hpp"
#include "bwn_coroutines/coroutineContext.hpp"

#if defined(DEBUG_ENABLED)

class bwn::CoroutineOperation::DebugCreatorInfo : public debug::CoroutineCreatorInfo
{
	//
	// Creation and destruction.
	//
public:
	DebugCreatorInfo(const StringId& _operationId)
		: debug::CoroutineCreatorInfo(String())
		, m_id( _operationId )
	{}
	virtual ~DebugCreatorInfo() override = default;

	//
	// Public interface.
	//
public:
	virtual String composeDescriptionV() const override
	{
		return bwn::format("Coroutine Operation by Id '{}'.", m_id);
	}
	void setId(const StringId& _id)
	{
		m_id = _id;
	}

	//
	// Private member.
	//
private:
	StringId m_id;
};

#endif

namespace names
{
	// This is mostly done, to not copy string each time we emmit signal.
	static constexpr const char* k_onUpdateSignal = "on_operation_update";
	static constexpr const char* k_onStartedSignal = "on_operation_started";
	static constexpr const char* k_onAbortedSignal = "on_operation_aborted";
	static constexpr const char* k_onFinishedSignal = "on_operation_finished";
	static constexpr const char* k_onChildrenFinishedSignal = "on_operation_children_finished";
} // namespace names

bwn::CoroutineOperation::CoroutineOperation()
	: m_result( nullptr )
	, m_semaphore( nullptr )
	, m_id( BWN_STRING_ID("PlainCoroutineOperation") )
	, m_state( EState::k_idle )
#if defined(DEBUG_ENABLED)
	, m_creatorInfo( std::make_shared<DebugCreatorInfo>(m_id) )
#endif
{}

bwn::CoroutineOperation::CoroutineOperation(const StringId& _operationId)
	: m_result( nullptr )
	, m_semaphore( nullptr )
	, m_id( _operationId )
	, m_state( EState::k_idle )
#if defined(DEBUG_ENABLED)
	, m_creatorInfo( std::make_shared<DebugCreatorInfo>(_operationId) )
#endif
{}

bwn::CoroutineOperation::~CoroutineOperation()
{
	if (m_state == EState::k_running && m_semaphore.is_valid())
	{
		m_semaphore->releaseSemaphore();
	}
}

void bwn::CoroutineOperation::_bind_methods()
{
	// Protected interface
	bindMethod<void(CoroutineOperation::*)(const Ref<CoroutineResult>&)>(D_METHOD("_exit_with_result", "result"), &CoroutineOperation::setResult);
	ClassDB::bind_method(D_METHOD("_exit_succeeded"), &CoroutineOperation::exitSucceeded);
	ClassDB::bind_method(D_METHOD("_exit_failed"), &CoroutineOperation::exitFailed);
	ClassDB::bind_method(D_METHOD("_abort"), &CoroutineOperation::abortOperation);

	// Public interface
	ClassDB::bind_method(D_METHOD("is_registered"), &CoroutineOperation::isRegistered);
	ClassDB::bind_method(D_METHOD("is_finished_running"), &CoroutineOperation::isFinishedRunning);
	ClassDB::bind_method(D_METHOD("is_aborted"), &CoroutineOperation::isAborted);
	ClassDB::bind_method(D_METHOD("is_started"), &CoroutineOperation::isStarted);
	ClassDB::bind_method(D_METHOD("is_running"), &CoroutineOperation::isRunning);
	ClassDB::bind_method(D_METHOD("is_paused"), &CoroutineOperation::isPaused);
	ClassDB::bind_method(D_METHOD("get_result"), &CoroutineOperation::getResult);
	ClassDB::bind_method(D_METHOD("set_semaphore"), &CoroutineOperation::setSemaphore);
	ClassDB::bind_method(D_METHOD("add_child_operation", "child_operation", "context"), &CoroutineOperation::addChildOperation);
	ClassDB::bind_method(D_METHOD("wait_operation", "operation"), &CoroutineOperation::waitOperation);
	ClassDB::bind_method(D_METHOD("set_id", "id_string"), &CoroutineOperation::setIdString);

	// Signals
	bwn::addSignal<CoroutineOperation, void(float)>(names::k_onUpdateSignal, "delta");
	bwn::addSignal<CoroutineOperation>(names::k_onStartedSignal);
	bwn::addSignal<CoroutineOperation>(names::k_onAbortedSignal);
	bwn::addSignal<CoroutineOperation>(names::k_onFinishedSignal);
	bwn::addSignal<CoroutineOperation>(names::k_onChildrenFinishedSignal);
}

void bwn::CoroutineOperation::abortOperation()
{
	CoroutineManager::getInstance()->abortOperation(this);
}

bool bwn::CoroutineOperation::isRegistered() const
{
	return m_state != EState::k_unregistered;
}

bool bwn::CoroutineOperation::isFinishedRunning() const
{
	return m_result.is_valid() && (m_state == EState::k_finished || m_state == EState::k_aborted);
}

bool bwn::CoroutineOperation::isStarted() const
{
	return m_state != EState::k_unregistered && m_state != EState::k_idle;
}

bool bwn::CoroutineOperation::isRunning() const
{
	return m_state == EState::k_running || m_state == EState::k_paused;
}

bool bwn::CoroutineOperation::isAborted() const
{
	return m_state == EState::k_aborted;
}

bool bwn::CoroutineOperation::isPaused() const
{
	return m_state == EState::k_paused;
}

Ref<bwn::CoroutineResult> bwn::CoroutineOperation::getResult() const
{
	return m_result;
}

void bwn::CoroutineOperation::setSemaphore(const Ref<CoroutineSemaphore>& _semaphore)
{
	const bool activelyRunning = m_state == EState::k_running;
	if (activelyRunning && m_semaphore.is_valid())
	{
		m_semaphore->releaseSemaphore();
	}

	m_semaphore = _semaphore;

	if (activelyRunning && m_semaphore.is_valid())
	{
		m_semaphore->forceLockSemaphore();
	}
}

Ref<bwn::CoroutineSemaphore> bwn::CoroutineOperation::getSemaphore() const
{
	return m_semaphore;
}

void bwn::CoroutineOperation::setResult(const Ref<CoroutineResult>& _result)
{
	m_result = _result;

#if defined(DEBUG_ENABLED)
	if (m_result.is_valid())
	{
		m_result->setOperationCreatorInfo(m_creatorInfo);
	}
#endif

	if (m_result.is_valid() && m_state == EState::k_paused)
	{
		// We are requiring queue rebuild from pause, because most certainly operation was finished from some callback, 
		// and because operation is paused, it would not be updated (and finished) until next queue rebuild.
		CoroutineManager::getInstance()->requireQueueRebuild();
	}

	m_state = m_result.is_valid() 
		? EState::k_finished
		: EState::k_idle;
}

void bwn::CoroutineOperation::setResult(Error _godotError, const String& _description)
{
	setResult(CoroutineResult::createResult(bwn::SystemFacility::createError(_godotError, _description)));
}

void bwn::CoroutineOperation::exitSucceeded()
{
	setResult(CoroutineResult::createSuccessResult());
}

void bwn::CoroutineOperation::exitFailed()
{
	setResult(Error::FAILED, "Generic failure.");
}

bool bwn::CoroutineOperation::addChildOperation(const Ref<CoroutineOperation>& _childOp, const Ref<CoroutineContext>& _context, bool _waitOperation) const
{
	BWN_ASSERT_WARNING_COND(_childOp.is_valid(), "Can't push nullptr operation.");
	if (_childOp.is_null()) 
	{
		return false;
	}

	static constexpr ECoroutineRelation k_simpleParentRelation = ECoroutineRelation::k_parent;
	static constexpr ECoroutineRelation k_waiterParentRelation = ECoroutineRelation(ECoroutineRelation::k_parent | ECoroutineRelation::k_waiter);

	const ECoroutineRelation relation = _waitOperation 
		? k_waiterParentRelation 
		: k_simpleParentRelation;
	
#if defined(DEBUG_ENABLED)
	Ref<CoroutineOperation> mutableOp = _childOp;
	mutableOp->setParentCreatorInfo(m_creatorInfo);
#endif

	return CoroutineManager::getInstance()->addOperation(_childOp, Ref<CoroutineOperation>(this), relation, _context);
}

bool bwn::CoroutineOperation::attachContext(const Ref<CoroutineContext>& _context) const
{
	return CoroutineManager::getInstance()->attachContext(Ref<CoroutineOperation>(this), _context);
}

void bwn::CoroutineOperation::waitOperation(const Ref<CoroutineOperation>& _operation) const
{
	CoroutineManager::getInstance()->pauseOperation(Ref<CoroutineOperation>(this), _operation);
}

void bwn::CoroutineOperation::setIdString(const String& _id)
{
	setId(StringId(bwn::wrap_view(_id)));
}

void bwn::CoroutineOperation::setId(const StringId& _id)
{
	m_id = _id;

#if defined(DEBUG_ENABLED)
	m_creatorInfo->setId(_id);
#endif
}

bwn::StringId bwn::CoroutineOperation::getId() const
{
	return m_id;
}

#if defined(DEBUG_ENABLED)
void bwn::CoroutineOperation::setParentCreatorInfo(std::shared_ptr<debug::CoroutineCreatorInfo> _info)
{
	m_creatorInfo->setParentCreator(_info);
}
std::shared_ptr<const bwn::debug::CoroutineCreatorInfo> bwn::CoroutineOperation::getCreatorInfo() const
{
	return m_creatorInfo;
}
#endif

void bwn::CoroutineOperation::registerOperation()
{
	if (m_state == EState::k_unregistered)
	{
		// The operation can't be registered twice.
		m_state = EState::k_idle;
		onRegistered();
	}
}

void bwn::CoroutineOperation::pause()
{
	if (m_state != EState::k_running)
	{
		// No reason to pause operation which is not running.
		return;
	}

	if (m_semaphore.is_valid())
	{
		m_semaphore->releaseSemaphore();
	}

	m_state = EState::k_paused;

	BWN_DEBUG_VERBOSE_LOG(
		"Operation {} (0x{:0>16x}) paused on state {}.",
		m_id,
		std::size_t(static_cast<CoroutineOperation*>(this)),
		bwn::wrap_utf(stateToStr(m_state)));
}

bool bwn::CoroutineOperation::unpause()
{
	if (m_state != EState::k_paused)
	{
		return true;
	}

	if (m_semaphore.is_valid() && !m_semaphore->tryLockSemaphore())
	{
		// We can continue only if semaphore is not valid, or if we successfully locked it.
		return false;
	}

	m_state = EState::k_running;
	
	BWN_DEBUG_VERBOSE_LOG(
		"Operation {} (0x{:0>16x}) unpaused in state {}.",
		m_id,
		std::size_t(static_cast<CoroutineOperation*>(this)),
		bwn::wrap_utf(stateToStr(m_state)));

	return true;
}

void bwn::CoroutineOperation::update(float _delta)
{
	onUpdateV(_delta);
	emit_signal(SNAME(names::k_onUpdateSignal), _delta);
}

void bwn::CoroutineOperation::abort()
{
	// We should not abort operation if it's already completed.
	if (isFinishedRunning()) {
		return;
	}

	const bool activelyRunning = m_state == EState::k_running;
	if (activelyRunning && m_semaphore.is_valid())
	{
		m_semaphore->releaseSemaphore();
		m_semaphore.unref();
	}

	BWN_DEBUG_LOG(
		"Operation {} (0x{:0>16x}) aborted from state {}.",
		m_id,
		std::size_t(static_cast<CoroutineOperation*>(this)),
		bwn::wrap_utf(stateToStr(m_state)));

	onAbortedV();
	emit_signal(SNAME(names::k_onAbortedSignal));

	setResult(CoroutineResult::createResult(bwn::SystemFacility::createError(Error::FAILED, L"Operation aborted.")));
	m_state = EState::k_aborted;
}

bool bwn::CoroutineOperation::start()
{
	BWN_ASSERT_WARNING_COND(m_state != EState::k_running, "Error starting operation, which is already running.");
	if (m_state == EState::k_running)
	{
		return true;
	}

	if (m_semaphore.is_valid() && !m_semaphore->tryLockSemaphore())
	{
		// We can continue only if semaphore is not valid, or if we successfully locked it.
		return false;
	}

	BWN_DEBUG_LOG("Operation {} (0x{:0>16x}) started.", m_id, std::size_t(this));

	m_result.unref();
	m_state = EState::k_running;
	onStartedV();
	emit_signal(SNAME(names::k_onStartedSignal));

	return true;
}

void bwn::CoroutineOperation::finish()
{
	const bool activelyRunning = m_state == EState::k_running;
	if (activelyRunning && m_semaphore.is_valid())
	{
		m_semaphore->releaseSemaphore();
		m_semaphore.unref();
	}

	BWN_DEBUG_LOG("Operation {} (0x{:0>16x}) finished with result {}.", m_id, std::size_t(this), m_result->getSystemResult());

	onFinishedV();
	emit_signal(SNAME(names::k_onFinishedSignal));
}

void bwn::CoroutineOperation::childrenFinished()
{
	BWN_DEBUG_VERBOSE_LOG("Operation {} (0x{:0>16x}) children finished running.", m_id, std::size_t(this));

	onChildrenFinishedV();
	emit_signal(SNAME(names::k_onChildrenFinishedSignal));
}

const char* bwn::CoroutineOperation::stateToStr(const EState _state)
{
	switch (_state)
	{
		case EState::k_unregistered: return "unregistered";
		case EState::k_idle: return "idle";
		case EState::k_running: return "running";
		case EState::k_aborted: return "aborted";
		case EState::k_finished: return "finished";
		case EState::k_paused: return "paused";
	}

	return "unknown";
}

void bwn::CoroutineOperation::onRegistered()
{}

void bwn::CoroutineOperation::onUpdateV(float)
{}

void bwn::CoroutineOperation::onStartedV()
{}

void bwn::CoroutineOperation::onChildrenFinishedV()
{}

void bwn::CoroutineOperation::onFinishedV()
{}

void bwn::CoroutineOperation::onAbortedV()
{}