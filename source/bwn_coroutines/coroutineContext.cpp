#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/coroutineContext.hpp"
#include "bwn_coroutines/coroutineResult.hpp"
#include "bwn_coroutines/coroutineManager.hpp"

namespace names
{
	// This is mostly done, to not copy string each time we emmit signal.
	static constexpr const char* k_onFinishedSignal = "on_finished";
} // namespace names

bwn::CoroutineContext::CoroutineContext()
	: m_callback( nullptr )
	, m_result()
	, m_running(false)
{}

bwn::CoroutineContext::CoroutineContext(FinishedCallback _callback)
	: m_callback( std::move(_callback) )
	, m_result()
	, m_running(false)
{}

bwn::CoroutineContext::~CoroutineContext() = default;

void bwn::CoroutineContext::_bind_methods()
{
	bwn::addSignal<CoroutineContext, void(Ref<CoroutineContext>)>(names::k_onFinishedSignal, "context");

	ClassDB::bind_method(D_METHOD("get_result"), &CoroutineContext::getResult);
}

void bwn::CoroutineContext::setCallback(FinishedCallback _callback)
{
	m_callback = std::move(_callback);
}

void bwn::CoroutineContext::abort()
{
	CoroutineManager::getInstance()->abortOperation(this);
}

void bwn::CoroutineContext::detach()
{
	CoroutineManager::getInstance()->detachContext(this);
	reset();
}

Ref<bwn::CoroutineResult> bwn::CoroutineContext::getResult() const
{
	return m_result;
}

void bwn::CoroutineContext::finish(const Ref<CoroutineResult>& _result)
{
	m_running = false;
	m_result = _result;

	// First native callback for the context itself.
	onFinishedV();

	// Next is callback for c++.
	if (m_callback)
	{
		m_callback(this);
	}

	// In the end callback for godot.
	emit_signal(SNAME(names::k_onFinishedSignal), this);
}

bool bwn::CoroutineContext::isRunning() const
{
	return m_running;
}

bool bwn::CoroutineContext::isIdle() const
{
	return !m_running && m_result.is_null();
}

bool bwn::CoroutineContext::isFinished() const
{
	return !m_running && m_result.is_valid();
}

void bwn::CoroutineContext::reset()
{
	m_running = false;
	m_result.unref();
}

void bwn::CoroutineContext::operationStarted()
{
	m_running = true;
	m_result.unref();
}

void bwn::CoroutineContext::onFinishedV()
{}