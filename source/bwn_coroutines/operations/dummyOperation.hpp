#pragma once

#include "bwn_coroutines/coroutineOperation.hpp"

namespace bwn
{
	// This is simply dummy operation, the whole point of which is to return specific result on the start.
	class CoroutineResult;

	class DummyOperation final : public CoroutineOperation
	{
		//
		// Construction and destruction
		//
	public:
		DummyOperation();
		DummyOperation(const Ref<CoroutineResult>& _result);
		virtual ~DummyOperation() override;

		//
		// Public interface.
		//
	public:
		void setDelayedResult(const Ref<CoroutineResult>& _result);

		//
		// Private methods.
		//
	private:
		virtual void onStartedV() override;

		//
		// Private members.
		//
	private:
		Ref<CoroutineResult> m_delayedResult;
	};
} // namespace bwn