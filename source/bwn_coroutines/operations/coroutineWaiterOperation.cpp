#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/operations/coroutineWaiterOperation.hpp"
#include "bwn_coroutines/operations/coroutineWaiterResult.hpp"
#include "bwn_coroutines/coroutineResult.hpp"

bwn::CoroutineWaiterOperation::CoroutineWaiterOperation()
	: CoroutineOperation( BWN_STRING_ID("WaiterCoroutine") )
	, m_operations()
	, m_initialOperationCount( 0 )
	, m_failedOperationCount( 0 )
{}

bwn::CoroutineWaiterOperation::CoroutineWaiterOperation(const StringId& _id)
	: CoroutineOperation( _id )
	, m_operations()
	, m_initialOperationCount( 0 )
	, m_failedOperationCount( 0 )
{}

bwn::CoroutineWaiterOperation::CoroutineWaiterOperation(std::vector<Ref<CoroutineOperation>> _operations)
	: CoroutineOperation( BWN_STRING_ID("WaiterCoroutine") )
	, m_operations( std::move(_operations) )
	, m_initialOperationCount( 0 )
	, m_failedOperationCount( 0 )
{
	BWN_TRAP_COND(
		!std::any_of(m_operations.begin(), m_operations.end(), [](const Ref<CoroutineOperation>& _op){ return _op.is_null(); }), 
		"Some of operations provided to stack are empty. This will definitely cause sigfaults later.");
}
bwn::CoroutineWaiterOperation::~CoroutineWaiterOperation() = default;

void bwn::CoroutineWaiterOperation::_bind_methods()
{

}

void bwn::CoroutineWaiterOperation::setTrackOperations(std::vector<Ref<CoroutineOperation>> _operations)
{
	BWN_TRAP_COND(
		!std::any_of(_operations.begin(), _operations.end(), [](const Ref<CoroutineOperation>& _op){ return _op.is_null(); }), 
		"Some of operations provided to stack are empty. This will definitely cause sigfaults later.");
	BWN_ASSERT_WARNING_COND(!isRegistered(), "Can't reset operations, because operation already running.");

	if (!isRegistered())
	{
		m_operations = std::move(_operations);
	}
}

void bwn::CoroutineWaiterOperation::addTrackOperation(const Ref<CoroutineOperation>& _operation)
{
	BWN_TRAP_COND(
		_operation.is_valid(), 
		"Operation provided to stack is empty. This will definitely cause sigfaults later.");
	BWN_ASSERT_WARNING_COND(!isRegistered(), "I don't think this is valid to add track operations after waiter was pushed to the stack.");

	m_operations.push_back(_operation);
	++m_initialOperationCount;
	
	if (!isRegistered())
	{
		waitOperation(_operation);
	}
}

void bwn::CoroutineWaiterOperation::onStartedV()
{
	m_initialOperationCount = m_operations.size();
	bool allFinished = true;
	for (const Ref<CoroutineOperation>& operation : m_operations)
	{
		if (!operation->isFinishedRunning())
		{
			allFinished = false;
			waitOperation(operation);
		}
	}
	
	if (allFinished)
	{
		// any_of checks if any of the operations inside are unfinished.
		// So the oposite will be, if there is no unfinished operations, then we exit.
		createWaiterResult();
	}
}
void bwn::CoroutineWaiterOperation::onUpdateV(float _delta)
{
	while(!m_operations.empty())
	{
		// In theory we should not get here untill all operations are finished,
		// but just to be sure, we will recheck all operations.
		if (!m_operations.back()->isFinishedRunning())
		{
			return;
		}

		m_failedOperationCount += m_operations.back()->getResult()->isFailed();
		m_operations.pop_back();
	}

	// If we are here, then we have ran out of operations.
	createWaiterResult();
}
void bwn::CoroutineWaiterOperation::onAbortedV()
{
	for (Ref<CoroutineOperation>& op : m_operations)
	{
		if (!op->isFinishedRunning())
		{
			op->abortOperation();
		}
	}
}

void bwn::CoroutineWaiterOperation::createWaiterResult()
{
	setResult(bwn::instantiateRef<CoroutineWaiterResult>(m_initialOperationCount, m_failedOperationCount));
}