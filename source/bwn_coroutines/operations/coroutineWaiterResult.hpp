#pragma once

#include "bwn_coroutines/coroutineResult.hpp"

namespace bwn
{
	// This is basically an operation, which waits for bunch of other operations to finish.
	class CoroutineWaiterResult : public CoroutineResult
	{
		GDCLASS(CoroutineWaiterResult, CoroutineResult);

		//
		// Construction and destruction.
		//
	public:
		CoroutineWaiterResult();
		CoroutineWaiterResult(uint32_t _totalOperationCount, uint32_t _failedOperationsCount);
		virtual ~CoroutineWaiterResult() override;

		//
		// Public interface.
		//
	public:
		void setupOperationCount(uint32_t _totalOperationCount, uint32_t _failedOperationsCount);

		//
		// Private members.
		//
	private:
		uint32_t m_totalOperationCount = 0;
		uint32_t m_failedOperationCount = 0;
	};
} // namespace bwn