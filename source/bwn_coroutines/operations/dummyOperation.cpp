#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/operations/dummyOperation.hpp"
#include "bwn_coroutines/coroutineResult.hpp"

bwn::DummyOperation::DummyOperation()
	: CoroutineOperation( BWN_STRING_ID("DummyOperation") )
	, m_delayedResult()
{}

bwn::DummyOperation::DummyOperation(const Ref<CoroutineResult>& _result)
	: CoroutineOperation( BWN_STRING_ID("DummyOperation") )
	, m_delayedResult( _result )
{}

bwn::DummyOperation::~DummyOperation() = default;

void bwn::DummyOperation::setDelayedResult(const Ref<CoroutineResult>& _result)
{
	BWN_ASSERT_ERROR_COND(!isRegistered(), "This is not correct to set the delayed result after the operation started running.");
	m_delayedResult = _result;
}

void bwn::DummyOperation::onStartedV()
{
	if (m_delayedResult.is_null())
	{
		exitSucceeded();
	}
	else
	{
		setResult(m_delayedResult);
	}
}