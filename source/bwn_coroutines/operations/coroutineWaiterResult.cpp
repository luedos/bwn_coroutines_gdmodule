#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/operations/coroutineWaiterResult.hpp"

bwn::CoroutineWaiterResult::CoroutineWaiterResult()
	: m_totalOperationCount( 0 )
	, m_failedOperationCount( 0 )
{}

bwn::CoroutineWaiterResult::CoroutineWaiterResult(uint32_t _totalOperationCount, uint32_t _failedOperationsCount)
	: m_totalOperationCount( _totalOperationCount )
	, m_failedOperationCount( _failedOperationsCount )
{
	BWN_ASSERT_WARNING_COND(m_failedOperationCount <= m_totalOperationCount, "The amount of failed operations from waiter is higher then total amount of all operations from waiter.");
}

bwn::CoroutineWaiterResult::~CoroutineWaiterResult() = default;

void bwn::CoroutineWaiterResult::setupOperationCount(uint32_t _totalOperationCount, uint32_t _failedOperationsCount)
{
	m_totalOperationCount = _totalOperationCount;
	m_failedOperationCount = _failedOperationsCount;
	BWN_ASSERT_WARNING_COND(m_failedOperationCount <= m_totalOperationCount, "The amount of failed operations from waiter is higher then total amount of all operations from waiter.");
}