#pragma once

#include "bwn_coroutines/coroutineOperation.hpp"

#include <vector>

namespace bwn
{
	class CoroutineResult;

	// This is basically an operation, which waits for bunch of other operations to finish.
	class CoroutineWaiterOperation : public CoroutineOperation
	{
		GDCLASS(CoroutineWaiterOperation, CoroutineOperation);

		//
		// Construction and destruction.
		//
	public:
		CoroutineWaiterOperation();
		CoroutineWaiterOperation(const StringId& _id);
		CoroutineWaiterOperation(std::vector<Ref<CoroutineOperation>> _operations);
		virtual ~CoroutineWaiterOperation() override;

		//
		// Godot methods.
		//
	public:
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		void setTrackOperations(std::vector<Ref<CoroutineOperation>> _operations);
		void addTrackOperation(const Ref<CoroutineOperation>& _operation);

		//
		// Protected members.
		//
	protected:
		virtual void onStartedV() override;
		virtual void onUpdateV(float _delta) override;
		virtual void onAbortedV() override;

		//
		// Private methods.
		//
	private:
		void createWaiterResult();

		//
		// Private members.
		//
	private:
		std::vector<Ref<CoroutineOperation>> m_operations;
		uint32_t m_initialOperationCount = 0;
		uint32_t m_failedOperationCount = 0;
	};
} // namespace bwn