#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/coroutineCreatorInfo.hpp"

#if defined(DEBUG_ENABLED)

bwn::debug::CoroutineCreatorInfo::CoroutineCreatorInfo(const String& _description)
	: m_description( _description )
{}

bwn::debug::CoroutineCreatorInfo::~CoroutineCreatorInfo() = default;

void bwn::debug::CoroutineCreatorInfo::setParentCreator(std::shared_ptr<CoroutineCreatorInfo> _relation)
{
	m_parentCreator = std::move(_relation);
}

String bwn::debug::CoroutineCreatorInfo::composeDescriptionV() const
{
	// For simple case, we only returning the description itself, but for the most drustic case, we could also compose description from additional info like operation or channel id.
	return m_description;
}

std::vector<String> bwn::debug::CoroutineCreatorInfo::composeCreationStack() const
{
	std::vector<String> ret;

	std::shared_ptr<const bwn::debug::CoroutineCreatorInfo> currentCreator = shared_from_this();
	while (currentCreator)
	{
		ret.push_back(currentCreator->composeDescriptionV());
		currentCreator = currentCreator->m_parentCreator;
	}

	return ret;
}

String bwn::debug::CoroutineCreatorInfo::composeFullDescription(const bwn::string_view& _separator) const
{
	// It is easier to use wstring for composing, because it actually has capacity, and String doesn't.
	std::u32string retBuffer;

	const std::vector<String> stack = composeCreationStack();

	bool first = true;
	for (const String& description : stack)
	{
		if (first)
		{
			first = false;
		}
		else
		{
			retBuffer.append(_separator.begin(), _separator.end());
		}

		retBuffer.append(description.get_data(), description.length());
	}

	return String(retBuffer.c_str(), retBuffer.length());
}

#endif