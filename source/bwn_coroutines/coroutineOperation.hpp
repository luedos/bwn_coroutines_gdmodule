#pragma once

#include "bwn_core/types/stringId.hpp"

#if defined(DEBUG_ENABLED)
	#include "bwn_coroutines/coroutineCreatorInfo.hpp"
	#include <memory>
#endif

#include <core/object/ref_counted.h>

namespace bwn
{
	class CoroutineManager;
	class CoroutineResult;
	class CoroutineContext;
	class CoroutineSemaphore;

	// The connection with the manager follows next rules:
	// start/finish (basically every method in private section accept 'abort') callbacks called exclusively from the manager update. 
	// 'abort' is also called from the manager, but the start point of this call is from operation or context abortOperation method. 
	// The manager simply manages re-translation of abort callback to all operation children down the stack.
	class CoroutineOperation : public RefCounted
	{	
		friend CoroutineManager;

		GDCLASS(CoroutineOperation, RefCounted);

		//
		// Private classes.
		//
	private:
		enum class EState
		{
			// State before added to manager.
			k_unregistered,
			// In this state operation added to the manager, but is not yet running.
			k_idle,
			// State before result is ready, or abort is called.
			k_running,
			// State when abort being called.
			k_aborted,
			// State after result.
			k_finished,
			// Waiting on semaphore.
			k_paused
		};
	#if defined(DEBUG_ENABLED)
		class DebugCreatorInfo;
	#endif

		//
		// Construction and destruction.
		//
	public:
		CoroutineOperation();
		// Default constructor. Accepts id, which is basically more specific operation type id.
		CoroutineOperation(const StringId& _id);
		// Default destructor.
		~CoroutineOperation() override;

		//
		// Godot methods.
		//
	protected:
		// Binds methods for the class.
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		// Aborts the operation.
		void abortOperation();
		// Returns true if operation already added to the manager.
		bool isRegistered() const;
		// Returns true if result is ready and operation was finished or aborted.
		bool isFinishedRunning() const;
		// Returns true if operation is on the stack and Start method was already called.
		bool isStarted() const;
		// Returns true if operation is running or paused (basically is it on the stack, started and not finished).
		bool isRunning() const;
		// Returns true if operation was aborted.
		bool isAborted() const;
		// Returns true if operation was running, but after that was paused.
		bool isPaused() const;
		// Returns the result of the operation.
		Ref<CoroutineResult> getResult() const;
		// Sets the semaphore for the operation. If operation is currently running, will do nothing.
		void setSemaphore(const Ref<CoroutineSemaphore>& _semaphore);
		// Just getter for the semaphore.
		Ref<CoroutineSemaphore> getSemaphore() const;
		// Adds child operation to the operation currently running.
		bool addChildOperation(const Ref<CoroutineOperation>& _childOp, const Ref<CoroutineContext>& _context, bool _waitOperation = false) const;
		// Attaches another context to operation. Succeed only if operation already was pushed to the stack.
		bool attachContext(const Ref<CoroutineContext>& _context) const;
		// Adds an operation to the stack, and pauses this operation waiting for it's execution to finish.
		void waitOperation(const Ref<CoroutineOperation>& _operation) const;
		// Returns operation purpose id.
		void setIdString(const String& _id);
		// Returns operation purpose id.
		void setId(const StringId& _id);
		// Returns operation purpose id.
		StringId getId() const;

	#if defined(DEBUG_ENABLED)
		void setParentCreatorInfo(std::shared_ptr<debug::CoroutineCreatorInfo> _info);
		std::shared_ptr<const debug::CoroutineCreatorInfo> getCreatorInfo() const;
	#endif

		//
		// Protected methods.
		//
	protected:
		// Sets the result of the operation.
		void setResult(const Ref<CoroutineResult>& _result);
		// Sets result as basic system result with description and godot flag.
		void setResult(Error _godotError, const String& _description);
		// Sets default result with flag "ok"
		void exitSucceeded();
		// Sets default result with flag "failed"
		void exitFailed();
		// This callback is called then operation first added to the stack (but not yet started).
		virtual void onRegistered();
		// Callback for operation update. Must be overrided! (default version only sets successfull result).
		virtual void onUpdateV(float _delta);
		// Callback for operation push. (Default version does nothing)
		virtual void onStartedV();
		// Callback then all children is finished running.
		virtual void onChildrenFinishedV();
		// Callback for operation deleting. (Default version does nothing)
		// Doesn't called if operation was aborted.
		virtual void onFinishedV();
		// Callback for operation aborting. (Default version does nothing)
		virtual void onAbortedV();

		//
		// Private methods.
		//
	private:
		// Nothing specific, this called right then operation is added to the manager, just before it started to run.
		void registerOperation();
		// Suspends operation from running and places it into the wating queue.
		void pause();
		// Unpauses operation. Returns true if semaphore was locked successfully.
		bool unpause();
		// Called by the manager every tick.
		void update(float _delta);
		// Called from a manager on operation abortion.
		void abort(); 
		// Called from a manager on operation start. Returns true if semaphore was locked successfully.
		bool start();
		// Called from a manager just before operation deletion. Is not called if operation was aborted.
		void finish();
		// Called from a manager when all children of current operation finished running.
		void childrenFinished();
		// A simple helper method to transform operation state into string.
		static const char* stateToStr(const EState _state);
		
		//
		// Private members.
		//
	private:
		// The result of the operation, which is populated in the end of the operation.
		Ref<CoroutineResult> m_result;
		// The semaphore which could be possibly used to chain operations.
		Ref<CoroutineSemaphore> m_semaphore;
		// Mostly used for the ease of debug.
		StringId m_id;
		// Current state of the operation.
		EState m_state = EState::k_idle;
	#if defined(DEBUG_ENABLED)
		// Operation creator info. Only used in debug.
		std::shared_ptr<DebugCreatorInfo> m_creatorInfo;
	#endif
	};
} // namespace bwn