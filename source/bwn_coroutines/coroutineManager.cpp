#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/coroutineManager.hpp"
#include "bwn_coroutines/coroutineResult.hpp"
#include "bwn_coroutines/coroutineOperation.hpp"
#include "bwn_coroutines/coroutineContext.hpp"
#include "bwn_coroutines/coroutineSemaphore.hpp"

class bwn::CoroutineManager::OperationContainer
{
	//
	// Construction and destruction.
	//
public:
	// Constructor from an operation (context is not really important, but operation is must be valid).
	// Channel id is also important, but if it were seted now as invalid, it wiil be reseted later with resetParent calls.
	OperationContainer(
		CoroutineManager& _manager,
		CoroutineChannel::ChannelId _channelId, 
		const Ref<CoroutineOperation>& _operation);
	// Constructor from operation and context.
	OperationContainer(
		CoroutineManager& _manager,
		CoroutineChannel::ChannelId _channelId, 
		const Ref<CoroutineOperation>& _operation, 
		const Ref<CoroutineContext>& _context);
	
	// Copy constructor.
	OperationContainer(const OperationContainer& _other) = delete;
	// Copy operator.
	OperationContainer& operator=(const OperationContainer& _other) = delete;

	// Move constructor.
	OperationContainer(OperationContainer&& _other) noexcept = delete;
	// Move operator.
	OperationContainer& operator=(OperationContainer&& _other) noexcept = delete;

	//
	// Public interface.
	//
public:
	// Just for convenience sake.
	OperationContainer* getParent() const;
	// Resets parent of the operation, and removes this operation from old parent children.
	// Returns true if parent was reseted, or already had the same parent.
	bool resetParent(OperationContainer* _newParent);
	// Checks if the current operation is in the same tree as other operation. 
	bool isParent(const Ref<CoroutineOperation>& _operation) const;
	// Checks if the current operation is in the same tree as other operation. 
	bool isParent(const OperationContainer* _container) const;

	// Appends context to the operation container.
	bool addContext(const Ref<CoroutineContext>& _context);
	// Removes specific context from the operation container.
	void removeContext(const Ref<CoroutineContext>& _context);

	// Adds operation as a child, and sets a parent for this operation (doesn't add operation to the current manager).
	bool addChild(OperationContainer& _child);
	// Removes operation from children and clears this operation parent (doesn't delete operation from it's manager).
	bool removeChild(OperationContainer& _child);

	// Adds operation which waits for the current operation.
	void addWaiter(OperationContainer& _waiter);
	// Removes operation which was waiting for the current operation.
	void removeWaiter(OperationContainer& _waiter);
	// Remove all waiters.
	void removeWaiters();
	// Returns true if operation in container is wating for some other operations.
	bool isWaiting() const;
	// Is specific operation waiting for this operation.
	bool isWaiter(const OperationContainer& _container) const;

	// Clears container, clears operation/parent/context, unlocks waiters, and aborts children.
	void clearContainer();

	// Returns container's main operation.
	const Ref<CoroutineOperation>& getOperation() const;
	// Returns actual context of the container.
	array_view<const Ref<CoroutineContext>> getContexts() const;
	// Returns all children of the container.
	array_view<OperationContainer*const> getChildren() const;
	// Returns channel id of the operation.
	CoroutineChannel::ChannelId getChannelId() const;

	bool popChildrenClearedFlag();

	// List node interface.
	OperationContainer* next(OperationContainer* _container);
	OperationContainer* next() const;
	OperationContainer* previous(OperationContainer* _container);
	OperationContainer* previous() const;

	//
	// Private methods.
	//
private:
	// Sets a new parent for the operation, but doesn't  removes this operatio from old parent children.
	void resetParentBasic(OperationContainer* _parent);
	// Adds operation as a child, but doesn't sets a parent for this operation.
	void addChildBasic(OperationContainer& _child);
	// Removes operation from children, but doesn't clears this operation parent.
	bool removeChildBasic(OperationContainer& _child);

	//
	// Private members.
	//
private:
	// Next container in the chain.
	OperationContainer* m_next;
	// Previous container in the chain.
	OperationContainer* m_previous;
	// Coroutine manager, to finish operation and stuff.
	CoroutineManager* m_manager;
	// Actual operation which will be updated.
	Ref<CoroutineOperation> m_operation;
	// Operation context.
	small_vector<Ref<CoroutineContext>, 4> m_contexts;
	// Parent operation.
	OperationContainer* m_parent;
	// Children operations.
	small_vector<OperationContainer*, 4> m_children;
	// Containers which wait for our operation to finish.
	small_vector<OperationContainer*, 4> m_waiters;
	// Id of the channel this operation is tied to.
	CoroutineChannel::ChannelId m_channelId;
	// The number of children this container currently wating.
	size_t m_waitedCount;
	// For now we will use simple bool instead of regular enum to track flags, but this could be changed in the future.
	bool m_childrenClearedFlag;
};

bwn::CoroutineManager::OperationContainer::OperationContainer(
	CoroutineManager& _manager,
	CoroutineChannel::ChannelId _channelId, 
	const Ref<CoroutineOperation>& _operation)
	: m_next( nullptr )
	, m_previous( nullptr )
	, m_manager( &_manager )
	, m_operation( _operation )
	, m_contexts()
	, m_parent( nullptr )
	, m_children()
	, m_waiters()
	, m_channelId( _channelId )
	, m_waitedCount( 0 )
	, m_childrenClearedFlag( false )
{
	BWN_TRAP_COND(m_operation.is_valid(), "Operation container was created with nullptr operation! Everyone panic!!");
	m_operation->registerOperation();
}

bwn::CoroutineManager::OperationContainer::OperationContainer(
	CoroutineManager& _manager,
	CoroutineChannel::ChannelId _channelId,
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineContext>& _context)
	: OperationContainer( _manager, _channelId, _operation )
{
	addContext(_context);
}

bwn::CoroutineManager::OperationContainer* bwn::CoroutineManager::OperationContainer::getParent() const
{
	return m_parent;
}

bool bwn::CoroutineManager::OperationContainer::resetParent(OperationContainer* _newParent)
{
	OperationContainer*const oldParent = getParent();
	if (_newParent == oldParent) {
		return true;
	}

	if (oldParent != nullptr)
	{
		oldParent->removeChildBasic(*this);
	}

	resetParentBasic(_newParent);

	if (_newParent != nullptr)
	{
		// No reason to check if container is valid, because we already checked it.
		_newParent->addChildBasic(*this);

		if (m_channelId == CoroutineChannel::k_invalidId) {
			m_channelId = _newParent->getChannelId();
		}
	}

	return true;
}

bool bwn::CoroutineManager::OperationContainer::isParent(const Ref<CoroutineOperation>& _operation) const
{
	if(!_operation.is_valid())
	{
		return false;
	}

	const OperationContainer*const container = m_manager->getOperationContainer(_operation);

	return isParent(container); 
}

bool bwn::CoroutineManager::OperationContainer::isParent(const OperationContainer* _container) const
{
	if(_container == nullptr)
	{
		return false;
	}

	return m_parent == _container || isParent(_container->m_parent); 
}

bool bwn::CoroutineManager::OperationContainer::addContext(const Ref<CoroutineContext>& _context)
{
	if (_context.is_valid() && std::find(m_contexts.begin(), m_contexts.end(), _context) == m_contexts.end())
	{
		m_contexts.push_back(_context);
		return true;
	}

	return false;
}

void bwn::CoroutineManager::OperationContainer::removeContext(const Ref<CoroutineContext>& _context)
{
	m_contexts.erase(
		std::remove(m_contexts.begin(), m_contexts.end(), _context),
		m_contexts.end());
}

bool bwn::CoroutineManager::OperationContainer::addChild(OperationContainer& _child)
{
	if (_child.isParent(this)) 
	{
		return true;
	}

	addChildBasic(_child);
	
	// If our new child had parent, we must clean it up before assigning ourself.
	if (OperationContainer* oldChildParent = _child.getParent())
	{
		oldChildParent->removeChildBasic(_child);
	}

	_child.resetParentBasic(this);

	return true;
}

bool bwn::CoroutineManager::OperationContainer::removeChild(OperationContainer& _child)
{
	if (!removeChildBasic(_child)) {
		return false;
	}

	// If we had this operation as a child, that mean that this operation must be on the stack..
	_child.resetParentBasic(nullptr);

	return true;
}

void bwn::CoroutineManager::OperationContainer::addWaiter(OperationContainer& _waiter)
{
	BWN_ASSERT_ERROR_COND(
		!_waiter.isWaiter(*this),
		"Recursive wait of {} for {} operation, this will basically cause a deadlock.", 
		_waiter.m_operation->getId(),
		m_operation->getId());

	// In theory, we have no reason to check if waiter was already added.
	// Most of the times we would not add operation second time, and even if we will,
	// this will not break anything.

	m_waiters.push_back(&_waiter);
	++_waiter.m_waitedCount;
}

void bwn::CoroutineManager::OperationContainer::removeWaiter(OperationContainer& _waiter)
{
	// Because we are not checking for duplicates on waiter addition, we can't stop erasing on first match.
	using WaiterIterator = small_vector<OperationContainer*, 4>::iterator;
	for (WaiterIterator it = m_waiters.begin(); it != m_waiters.end(); )
	{
		if (*it == &_waiter)
		{
			// We should decrement count each time, because we incremented it each time on addition.
			BWN_ASSERT_ERROR_COND(
				_waiter.m_waitedCount != 0, 
				"Operation {} is decrementing waiting count of {} when it's 0.",
				m_operation->getId(),
				_waiter.m_operation->getId());

			--_waiter.m_waitedCount;

			it = m_waiters.erase(it);
			
			continue;
		}

		++it;
	}
}

void bwn::CoroutineManager::OperationContainer::removeWaiters()
{
	while(!m_waiters.empty())
	{
		OperationContainer*const waiter = m_waiters.back();

		BWN_ASSERT_ERROR_COND(
			waiter->m_waitedCount != 0, 
			"Operation {} is decrementing waiting count of {} when it's 0.",
			m_operation->getId(),
			waiter->m_operation->getId());

		--waiter->m_waitedCount;

		m_waiters.pop_back();
	}
}

bool bwn::CoroutineManager::OperationContainer::isWaiting() const
{
	return m_waitedCount > 0;
}

bool bwn::CoroutineManager::OperationContainer::isWaiter(const OperationContainer& _container) const
{
	for (const OperationContainer*const waiter : m_waiters)
	{
		if (&_container == waiter || waiter->isWaiter(_container))
		{
			return true;
		}
	}

	return false;
}

void bwn::CoroutineManager::OperationContainer::clearContainer()
{
	// Processing all operation children.
	while(!m_children.empty())
	{
		OperationContainer*const child = m_children.back();
		removeChild(*child);
		// In theory, operation must be already finished or at least aborted, but just in case..
		// If it were already aborted or finished, then this will do nothing.
		m_manager->abortOperation(*child);
	}

	removeWaiters();

	// If operation has no parent, this will do nothing.
	resetParent(nullptr);

	m_operation.unref();
	m_contexts.clear();
}

bwn::CoroutineChannel::ChannelId bwn::CoroutineManager::OperationContainer::getChannelId() const
{
	return m_channelId;
}

bool bwn::CoroutineManager::OperationContainer::popChildrenClearedFlag()
{
	const bool flag = m_childrenClearedFlag;
	m_childrenClearedFlag = false;
	return flag;
}

const Ref<bwn::CoroutineOperation>& bwn::CoroutineManager::OperationContainer::getOperation() const
{
	return m_operation;
}

bwn::array_view<const Ref<bwn::CoroutineContext>> bwn::CoroutineManager::OperationContainer::getContexts() const
{
	return m_contexts;
}

bwn::array_view<bwn::CoroutineManager::OperationContainer*const> bwn::CoroutineManager::OperationContainer::getChildren() const
{
	return m_children;
}

bwn::CoroutineManager::OperationContainer* bwn::CoroutineManager::OperationContainer::next(OperationContainer* _container)
{
	OperationContainer*const ret = m_next;
	m_next = _container;
	return ret;
}

bwn::CoroutineManager::OperationContainer* bwn::CoroutineManager::OperationContainer::next() const
{
	return m_next;
}

bwn::CoroutineManager::OperationContainer* bwn::CoroutineManager::OperationContainer::previous(OperationContainer* _container)
{
	OperationContainer*const ret = m_previous;
	m_previous = _container;
	return ret;
}

bwn::CoroutineManager::OperationContainer* bwn::CoroutineManager::OperationContainer::previous() const
{
	return m_previous;
}

void bwn::CoroutineManager::OperationContainer::resetParentBasic(OperationContainer* _parent)
{
	m_parent = _parent;
}

void bwn::CoroutineManager::OperationContainer::addChildBasic(OperationContainer& _child)
{
	m_children.push_back(&_child);
	m_childrenClearedFlag = false;
}

bool bwn::CoroutineManager::OperationContainer::removeChildBasic(OperationContainer& _child)
{
	const small_vector<OperationContainer*, 4>::reverse_iterator childIt = std::find_if(
		m_children.rbegin(), 
		m_children.rend(),
		[&_child](const OperationContainer* _checkChild)
		{
			return _checkChild == &_child;
		});
	const bool hadChild = childIt != m_children.rend();

	BWN_ASSERT_WARNING_COND(
		hadChild, 
		"Can't remove child {} from {} operation, because it's not in children.",
		_child.m_operation->getId(),
		m_operation->getId());

	if (hadChild)
	{
		m_children.erase((childIt + 1).base());
		m_childrenClearedFlag = m_children.empty();
	}

	return hadChild;
}

bwn::CoroutineManager::CoroutineManager()
	: m_containerStack()
	, m_executionQueue()
	, m_queueRefreshRequired( true )
{}

bwn::CoroutineManager::~CoroutineManager()
{
	clearOperations();
}

void bwn::CoroutineManager::update(float _delta)
{
	if (m_queueRefreshRequired)
	{
		rebuildExecutionQueue();
		m_queueRefreshRequired = false;
	}

	for (QueueIterator queueIt = m_executionQueue.begin(); queueIt != m_executionQueue.end();)
	{
		CoroutineOperation*const operation = *queueIt;

		if (operation->isRunning())
		{
			operation->update(_delta);
		}
		else if (!operation->isStarted())
		{
			if (!operation->start())
			{
				// If the operation failed to start due to semaphore, removing operation from the queue for now.
				queueIt = m_executionQueue.erase(queueIt);
				// No need to check for finished.
				continue;
			}

			// If start was successful..
			OperationContainer*const containerIt = getOperationContainer(*operation, queueIt - m_executionQueue.begin());
			const bwn::array_view<const Ref<CoroutineContext>> contexts = containerIt->getContexts();
			for (const Ref<CoroutineContext>& context : contexts)
			{
				Ref<CoroutineContext> mutableContext = context;
				mutableContext->operationStarted();
			}
		}

		if (operation->isFinishedRunning())
		{
			StackIterator containerIt = getOperationContainer(*operation, queueIt - m_executionQueue.begin());			
			queueIt = eraseContainer(queueIt, containerIt);
			continue;
		}

		++queueIt;
	}
}

bool bwn::CoroutineManager::addOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineContext>& _context,
	CoroutineChannel::ChannelId _channelId)
{
	return addOperation(_operation, nullptr, ECoroutineRelation::k_none, _context, _channelId);
}

bool bwn::CoroutineManager::addOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineOperation>& _relativeOperation,
	ECoroutineRelation _relationMode,
	const Ref<CoroutineContext>& _context,
	CoroutineChannel::ChannelId _channelId)
{
#if defined(DEBUG_ENABLED)
	if (_context.is_valid())
	{
		OperationContainer*const debugContextContainer = getOperationContainer(_context);
		BWN_ASSERT_ERROR_COND(
			debugContextContainer == nullptr,
			"Adding operation {} with context which is already connected to a different operation {}, this is ub.",
			_operation->getId(),
			debugContextContainer->getOperation()->getId());
	}

	if (_operation.is_valid())
	{
		OperationContainer*const debugOperationContainer = getOperationContainer(*_operation.ptr());
		BWN_ASSERT_ERROR_COND(
			debugOperationContainer == nullptr,
			"Adding operation {} which is already added to the manager, this is ub.",
			_operation->getId());
	}
#endif

	OperationContainer*const container = m_containerStack.emplace_back( *this, _channelId, _operation, _context );
	BWN_TRAP_COND(container != nullptr, "Error creating coroutine container!");

	do
	{
		BWN_ASSERT_WARNING_COND(
			// Both of those should be true, or both should be false.
			(_relativeOperation.is_null() == (_relationMode == ECoroutineRelation::k_none)), 
			"Adding operation {} with relation to {}, but no relation flag was specified.",
			_operation->getId(),
			_relativeOperation->getId());

		if (_relativeOperation.is_null() || _relationMode == ECoroutineRelation::k_none) {
			break;
		}

		OperationContainer*const relationContainer = getOperationContainer(_relativeOperation);
		BWN_ASSERT_WARNING_COND(
			relationContainer != nullptr, 
			"Adding operation with relation to {} operation which is not on the stack. Can't establish relation.",
			_operation->getId(),
			_relativeOperation->getId());

		if (relationContainer == nullptr) {
			break;
		}

		if (_relationMode & ECoroutineRelation::k_parent)
		{	
			container->resetParent(relationContainer);
		}

		if (_relationMode & ECoroutineRelation::k_waiter)
		{
			container->addWaiter(*relationContainer);
		}
	} while (false);
	

	BWN_ASSERT_WARNING_COND(
		container->getChannelId() != CoroutineChannel::k_invalidId,
		"While adding operation {} no channel id was specified. This is unadvised.",
		_operation->getId());

	// If everything is ok, we will clear context from result and any flags.
	if (_context.is_valid()) 
	{
		// Very stupid, but const reference behave itself as a reference to const class.
		// Because of this we still technically can call mutable methods on it, 
		// but we need to copy it first.
		// In good design, even if reference is const itself, the class it represents would not be const.
		Ref<CoroutineContext> copyContext = _context;
		copyContext->reset();
	}

	BWN_DEBUG_VERBOSE_LOG(
		"Operation {} (0x{:0>16x}) pushed into the stack:\n\t> {}",
		_operation->getId(),
		std::size_t(_operation.ptr()),
		_operation->getCreatorInfo()->composeFullDescription(U"\n\t> "));

	requireQueueRebuild();

	return true;
}

void bwn::CoroutineManager::pauseOperation(
	const Ref<CoroutineOperation>& _pausedOperation, 
	const Ref<CoroutineOperation>& _operationToWait)
{
	BWN_ASSERT_WARNING_COND(_pausedOperation.is_valid(), "Can't pause nullptr operation.");
	BWN_ASSERT_WARNING_COND(
		_operationToWait.is_valid(),
		"Trying to pause {} operation but no operation to wait was specified.",
		_pausedOperation.is_valid() ? _pausedOperation->getId() : StringId());

	if (_pausedOperation.is_null() || _operationToWait.is_null())
	{
		return;
	}

	OperationContainer*const pausedContainer = getOperationContainer(**_pausedOperation);
	BWN_ASSERT_WARNING_COND(pausedContainer != nullptr, "Operation {} required to pause wasn't added to the operation stack.", _pausedOperation->getId());
	if (pausedContainer == nullptr)
	{
		return;
	}

	OperationContainer*const containerToWait = getOperationContainer(**_operationToWait);
	BWN_ASSERT_WARNING_COND(
		containerToWait != nullptr, 
		"Operation {} for which operation {} is required to wait wasn't added to the stack.",
		_operationToWait->getId(),
		_pausedOperation->getId());

	if (containerToWait == nullptr)
	{
		return;
	}

	containerToWait->addWaiter(*pausedContainer);
	
	requireQueueRebuild();
}

bwn::CoroutineManager::QueueIterator bwn::CoroutineManager::eraseContainer(QueueIterator _queueIt, StackIterator _containerIt)
{
#if defined(DEBUG_ENABLED)
	CoroutineOperation*const operationPtr = *_queueIt;
	BWN_TRAP_COND(operationPtr != nullptr, "Error erasing container with nullptr operation.");
	BWN_TRAP_COND(
		_containerIt->getOperation() == operationPtr,
		"Optimized method of deleting operation from execution queue and the container from stack, deletes container which operation doesn't correspond to the operation from queue being deleted. Operation from queue: {}, operation from stack it: {}.",
		_queueIt != m_executionQueue.end() ? (*_queueIt)->getId() : StringId(),
		_containerIt != m_containerStack.end() ? _containerIt->getOperation()->getId() : StringId());
#endif

	eraseContainer(_containerIt);
	return m_executionQueue.erase(_queueIt);
}

void bwn::CoroutineManager::eraseContainer(StackIterator _containerIt)
{
	const Ref<CoroutineOperation> operation = _containerIt->getOperation();
	small_vector<Ref<CoroutineContext>, 8> contexts;
	{
		const array_view<const Ref<CoroutineContext>> contextsView = _containerIt->getContexts();
		contexts.reserve(contextsView.size());
		contexts.assign(contextsView.begin(), contextsView.end());
	}

	_containerIt->clearContainer();
	m_containerStack.erase(_containerIt);
	finishOperation(operation, contexts);

	// Update rest containers states.
	updateContainersState();

	BWN_DEBUG_VERBOSE_LOG(
		"Operation {} (0x{:0>16x}) removed from stack.",
		operation->getId(),
		std::size_t(operation.ptr()));

	requireQueueRebuild();
}

void bwn::CoroutineManager::finishOperation(
	const Ref<CoroutineOperation>& _operation,
	const array_view<Ref<CoroutineContext>>& _contexts)
{
	Ref<CoroutineOperation> mutableOperation = _operation;

	// We should not call finish on operation which was aborted, because aborted operation not counts as finished.
	if (mutableOperation->isFinishedRunning() && !mutableOperation->isAborted())
	{
		mutableOperation->finish();
	}
	else if (mutableOperation->isRunning())
	{
		// If operation running, when erasing container was done by some external force (like manager deleting).
		// In that case it only makes sense to abort operation.
		// Also there is no need to abort children yet, because we will abort them later in the function.
		mutableOperation->abort();
	}

	for (Ref<CoroutineContext>& context : _contexts)
	{
		context->finish(mutableOperation->getResult());
	}
}

void bwn::CoroutineManager::clearOperations()
{
	while(!m_containerStack.empty())
	{
		// Erasing from the end, so we would not need to copy containers all the time.
		eraseContainer(m_containerStack.begin());
	}

	requireQueueRebuild();
}

bwn::CoroutineManager::OperationContainer* bwn::CoroutineManager::getOperationContainer(
	const CoroutineOperation& _operation, 
	size_t _containerStartIndexHint)
{
	StackIterator operationIt = m_containerStack.begin();
	while(_containerStartIndexHint != 0)
	{
		++operationIt;
		--_containerStartIndexHint;
	}

	const StackIterator end = m_containerStack.end();
	while(operationIt != end)
	{
		OperationContainer& container = *operationIt;
		if (container.getOperation().ptr() == &_operation)
		{
			return &container;
		}

		++operationIt;
	}

	return nullptr;
}

bwn::CoroutineManager::OperationContainer* bwn::CoroutineManager::getOperationContainer(const Ref<CoroutineContext>& _context)
{
	for (StackIterator operationIt = m_containerStack.begin(), endIt = m_containerStack.end(); operationIt != endIt; ++operationIt)
	{
		OperationContainer& container = *operationIt;

		const array_view<const Ref<CoroutineContext>> contexts = container.getContexts();
		if (std::find(contexts.begin(), contexts.end(), _context) != contexts.end())
		{
			return &container;
		}
	}
	
	return nullptr;
}

void bwn::CoroutineManager::abortOperation(const Ref<CoroutineOperation>& _operation)
{
	if (_operation.is_null()) {
		return;
	}

	OperationContainer*const container = getOperationContainer(**_operation, 0);
	
	if (container == nullptr) {
		return;
	}

	abortOperation(*container);
}

void bwn::CoroutineManager::abortOperation(const Ref<CoroutineContext>& _context)
{
	OperationContainer*const container = getOperationContainer(_context);
	if (container == nullptr) {
		return;
	}

	abortOperation(*container);
}

void bwn::CoroutineManager::abortOperation(OperationContainer& _container)
{
	for (OperationContainer*const child : _container.getChildren()) {
		abortOperation(*child);
	}
	
	// Operation has inner check to not be aborted second time.
	Ref<CoroutineOperation> operation = _container.getOperation();
	operation->abort();

	requireQueueRebuild();
}

void bwn::CoroutineManager::abortOperations(CoroutineChannel::ChannelId _channelId)
{
	for(OperationContainer& container : m_containerStack)
	{
		if (container.getChannelId() == _channelId) {
			abortOperation(container);
		}
	}
}

bool bwn::CoroutineManager::attachContext(const Ref<CoroutineOperation>& _operation, const Ref<CoroutineContext>& _context)
{
	OperationContainer*const container = getOperationContainer(_operation);
	BWN_ASSERT_WARNING_COND(container != nullptr, "Can't attach context to operation which doesn't exist in the stack.");

	if (container != nullptr && container->addContext(_context))
	{
		Ref<CoroutineContext> mutableContext = _context;
		if (container->getOperation()->isRunning())
		{
			mutableContext->operationStarted();
		}
		else
		{
			mutableContext->reset();
		}

		return true;
	}

	return false;
}

void bwn::CoroutineManager::detachContext(const Ref<CoroutineContext>& _context)
{
	OperationContainer*const container = getOperationContainer(_context);
	BWN_ASSERT_WARNING_COND(container != nullptr, "Can't detach context which doesn't connected to any of operations.");

	if (container != nullptr)
	{
		container->removeContext(_context);
	}
}

std::vector<Ref<bwn::CoroutineOperation>> bwn::CoroutineManager::getOperations(CoroutineChannel::ChannelId _channelId) const
{
	std::vector<Ref<CoroutineOperation>> res;

	for (const OperationContainer& container : m_containerStack)
	{
		if (container.getChannelId() == _channelId)
		{
			res.push_back(container.getOperation());
		}
	}

	return res;
}

void bwn::CoroutineManager::rebuildExecutionQueue()
{
	m_executionQueue.clear();

	for (const OperationContainer& container : m_containerStack)
	{
		// This loop basically checks every container if it can be added to the execution queue.
		// (so every 'if' check you see here will check if we could skip operation and not add it to the execution queue).

		Ref<CoroutineOperation> operation = container.getOperation();
		Ref<CoroutineSemaphore> semaphore = operation->getSemaphore();

		const bool isActivelyRunning = operation->isRunning() && !operation->isPaused();
		const bool waitingOperations = container.isWaiting();

		if (isActivelyRunning)
		{
			const bool semathoreDebt = semaphore.is_valid() && semaphore->getTickets() < 0;
			const bool shouldPause = waitingOperations || semathoreDebt;
			if (shouldPause)
			{
				// If operation is actively running, but should be paused, then we pause it from here, and not adding it the queue.
				operation->pause();
				continue;
			}
		}

		if (operation->isPaused())
		{
			// If operation is paused, we are trying to unpause it.
			if (container.isWaiting() || !operation->unpause())
			{
				// If operation is still waiting on other operations, or we failed in unpausing the operation, we just skip it.
				continue;
			}
		}

		if (!operation->isStarted())
		{
			// If operation is not started, but still waiting on other operations, we can't add this operation.
			// We are not checking for semaphore here, because this check will be performed on 'start' call of the operation automatically.
			if (waitingOperations)
			{
				continue;
			}
		}

		// If the operation is finished running, it will fail all the checks above, and so added to the queue.

		m_executionQueue.push_back(operation.ptr());
	}
}

void bwn::CoroutineManager::requireQueueRebuild()
{
	m_queueRefreshRequired = true;
}

void bwn::CoroutineManager::updateContainersState()
{
	for (OperationContainer& operationContainer : m_containerStack)
	{
		Ref<CoroutineOperation> operation = operationContainer.getOperation();
		if (operationContainer.popChildrenClearedFlag())
		{
			operation->childrenFinished();
		}
	}
}

void bwn::CoroutineManagerUpdate::_notification(int _notification)
{
	if (_notification == NOTIFICATION_PROCESS)
	{
		CoroutineManager::getInstance()->update(get_process_delta_time());
	}
}

bwn::CoroutineManagerUpdate::CoroutineManagerUpdate()
{
	set_process(true);
}
bwn::CoroutineManagerUpdate::~CoroutineManagerUpdate() = default;
