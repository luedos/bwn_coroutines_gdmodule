#pragma once

#include "bwn_core/functional/callback.hpp"

#include <core/object/ref_counted.h>

namespace bwn
{
	class CoroutineManager;
	class CoroutineResult;

	class CoroutineContext : public RefCounted
	{	
		friend CoroutineManager;

		GDCLASS(CoroutineContext, RefCounted);

		//
		// Public aliasing.
		//
	public:
		// Callback for the finished operation.
		using FinishedCallback = bwn::callback<void(Ref<CoroutineContext>)>;

		//
		// Construction and destruction.
		//
	public:
		// Default constructor.
		CoroutineContext();
		// Constructor to automatically setup finish callback.
		CoroutineContext(FinishedCallback _callback);
		// Default destructor.
		~CoroutineContext() override;

		//
		// Godot methods.
		//
	protected:
		// Binds methods for the class.
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		// Sets a callback for then an Coroutine operation is finished
		void setCallback(FinishedCallback _callback);
		// Aborts an operation this context is tied to.
		void abort();
		// Detaches context from operation running. The context will not get any callback from operation finished.
		void detach();
		// Returns result of the operation. Can be not seted, if operation is not finished.
		Ref<CoroutineResult> getResult() const;
		// The function called on the operation finished from Coroutine manager.
		void finish(const Ref<CoroutineResult>& _result);
		// Returns true if context's operation is currently running.
		bool isRunning() const;
		// Returns true if context is idle (not runned yet).
		bool isIdle() const;
		// Returns true if context is finished and result seted.
		bool isFinished() const;
		// Resets result and 'running' flag, but doesn't resets the callback.
		void reset();

		//
		// Private methods.
		//
	private:
		// Basically this is the signal from the coroutine manager that operation is started to run.
		void operationStarted(); 
		// Virtual callback which is called on the operation finished.
		virtual void onFinishedV();

		//
		// Private members.
		//
	private:
		// Callback for operation finished.
		FinishedCallback m_callback;
		// Result of the operation.
		Ref<CoroutineResult> m_result;
		// If true, the context is currently running.
		bool m_running = false;
	};
} // namespace bwn