#pragma once

#include "bwn_coroutines/coroutineChannel.hpp"
#include "bwn_coroutines/coroutineRelation.hpp"
#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/types/singleton.hpp"
#include "bwn_core/containers/constaddrDequeAllocator.hpp"
#include "bwn_core/containers/simpleList.hpp"

#include <scene/main/node.h>

#include <vector>

namespace bwn
{
	class CoroutineOperation;
	class CoroutineContext;
	class CoroutineSemaphore;
	class CoroutineManagerUpdate;

	// This class shuld be declared as a singleton in godot itself.
	class CoroutineManager : public ManualSingleton<CoroutineManager>
	{
		// This is mostly needed for the abortOperation/AbortContext calls.
		friend CoroutineOperation;
		friend CoroutineContext;
		// This is mostly because coroutine channel is actually an interface for the manager.
		friend CoroutineChannel;
		friend CoroutineSemaphore;
		// This is so we could have update in the seperate class.
		friend CoroutineManagerUpdate;

		//
		// Private structures.
		//
	private:
		class OperationContainer;
		using StackAllocator = bwn::simple_list<OperationContainer, bwn::constaddr_deque_allocator<OperationContainer, 32>>;
		using StackIterator = StackAllocator::iterator;
		using ExecutionQueue = std::vector<CoroutineOperation*>;
		using QueueIterator = ExecutionQueue::iterator;

		//
		// Construction and destruction.
		//
	public:
		// Default constructor.
		CoroutineManager();
		// Default destructor.
		~CoroutineManager();

		//
		// Public interface.
		//
	public:
		// Updates an operations.
		void update(float _delta);

		//
		// Private methods.
		//
	private:
		// Adds an operation to the stack.
		bool addOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineContext>& _context,
			CoroutineChannel::ChannelId _channelId);
		// Adds an operation to the stack with parent. If operation id is invalid, it will use parent operation id.
		bool addOperation(
			const Ref<CoroutineOperation>& _operation, 
			const Ref<CoroutineOperation>& _relativeOperation,
			ECoroutineRelation _relationMode,
			const Ref<CoroutineContext>& _context,
			CoroutineChannel::ChannelId _channelId = CoroutineChannel::k_invalidId);
		// Makes specific opeartion to wait for another one.
		void pauseOperation(const Ref<CoroutineOperation>& _pausedOperation, const Ref<CoroutineOperation>& _operationToWait);
		// Returns iterator from operation stack of the container which holds required context.
		OperationContainer* getOperationContainer(const CoroutineOperation& _opration, size_t _containerStartIndexHint = 0);
		// Returns iterator from operation stack of the container which holds required context.
		OperationContainer* getOperationContainer(const Ref<CoroutineContext>& _context);
		// This is more or less just an optimization thing, to not search stack iterator.
		QueueIterator eraseContainer(QueueIterator _queueIt, StackIterator _containerIt);
		// Removes operation from manager.
		void eraseContainer(StackIterator _containerIt);
		// Finishes operation and it's context. (mostly just a helper function).
		static void finishOperation(const Ref<CoroutineOperation>& _operation, const bwn::array_view<Ref<CoroutineContext>>& _contexts);
		// Clears all operations.
		void clearOperations();
		// Aborts a specific operation and all it's children.
		void abortOperation(const Ref<CoroutineOperation>& _operation);
		// Aborts an operation tied by this context and all it's children.
		void abortOperation(const Ref<CoroutineContext>& _context);
		// Basically a helper for inner usage. Aborts operation and all it's children.
		void abortOperation(OperationContainer& _container);
		// Aborts all operations from specific channel.
		void abortOperations(CoroutineChannel::ChannelId _channelId);
		// Additionally attaches context to already running operation.
		bool attachContext(const Ref<CoroutineOperation>& _operation, const Ref<CoroutineContext>& _context);
		// Detaches context from the operation it running.
		void detachContext(const Ref<CoroutineContext>& _context);
		// Returns all operations for specific channel.
		std::vector<Ref<CoroutineOperation>> getOperations(CoroutineChannel::ChannelId _channelId) const;
		// Forces the execution queue rebuild.
		void rebuildExecutionQueue();
		// Requeres rebuild of execution queue.
		void requireQueueRebuild();
		// Pops container state flags, and updates them accordingly.
		void updateContainersState();

		//
		// Private members.
		//
	private:
		// Stack of all operations.
		StackAllocator m_containerStack;
		// The queue of the execution.
		ExecutionQueue m_executionQueue;
		// Basically a lock, just to be sure queue will not be rebuild in the process of update.
		bool m_queueRefreshRequired = false;
	};

	class CoroutineManagerUpdate : public Node, public AutoSingleton<CoroutineManagerUpdate>
	{
		GDCLASS(CoroutineManagerUpdate, Node);

		//
		// Godot methods.
		//
	private:
		void _notification(int _notification);

		//
		// Construction and destruction.
		//
	public:
		CoroutineManagerUpdate();
		virtual ~CoroutineManagerUpdate() override;
	};
} // namespace bwn