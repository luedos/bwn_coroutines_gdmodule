#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/coroutineSemaphore.hpp"
#include "bwn_coroutines/coroutineManager.hpp"

bwn::CoroutineSemaphore::CoroutineSemaphore()
	: m_tickets(1)
	, m_maxTickets(1)
{}

bwn::CoroutineSemaphore::CoroutineSemaphore(int _tickets)
	: m_tickets(_tickets)
	, m_maxTickets(_tickets)
{}

bwn::CoroutineSemaphore::~CoroutineSemaphore()
{
	BWN_ASSERT_WARNING_COND(
		m_tickets == m_maxTickets, 
		"Destroying semaphore with some tickets not returned. Something bad happened. Current tickets: {}. Max tickets: {}.", 
		m_tickets, 
		m_maxTickets);
}

void bwn::CoroutineSemaphore::_bind_methods()
{
	ClassDB::bind_method(D_METHOD("set_max_tickets"), &CoroutineSemaphore::setTickets);
}

void bwn::CoroutineSemaphore::setTickets(int _tickets)
{
	const int currentLockCount = m_maxTickets - m_tickets;

	const bool wasEmpty = m_tickets == 0; 

	m_maxTickets = _tickets;
	m_tickets = _tickets - currentLockCount;

	const bool canAddOperations = wasEmpty && m_tickets > 0;
	const bool shouldPauseOperations = m_tickets < 0;
	if (canAddOperations || shouldPauseOperations)
	{
		CoroutineManager::getInstance()->requireQueueRebuild();
	}

}
void bwn::CoroutineSemaphore::forceLockSemaphore()
{
	--m_tickets;
}

bool bwn::CoroutineSemaphore::tryLockSemaphore()
{
	if (m_tickets > 0)
	{
		--m_tickets;
		return true;
	}

	return false;
}

void bwn::CoroutineSemaphore::releaseSemaphore()
{
	BWN_ASSERT_WARNING_COND(m_tickets != m_maxTickets, "Releasing semaphore when tickets are full. Something went wrong!");
	if (m_tickets < m_maxTickets)
	{
		++m_tickets;
	}
}

int bwn::CoroutineSemaphore::getTickets() const
{
	return m_tickets;
}