#pragma once

#include <core/object/ref_counted.h>

namespace bwn
{
	// The semaphore class is very simple.
	// All it has is count of tickets, which is decremented each time,
	// the operation goes from idle to running state.
	// If there is no tickets left, the operation will remain idle.
	// This behavior is controlled in coroutine manager.
	class CoroutineSemaphore : public RefCounted
	{
		GDCLASS(CoroutineSemaphore, RefCounted);

		//
		// Construction and destruction.
		//
	public:
		CoroutineSemaphore();
		CoroutineSemaphore(int _tickets);
		virtual ~CoroutineSemaphore() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		// Sets number max number of tickets which can be locked by operations.
		void setTickets(int _tickets);
		// Forces lock semaphore. This could lead to the negative amount of tickets.
		void forceLockSemaphore();
		// Tries to lock the semaphore. Returns false if no tickets left.
		bool tryLockSemaphore();
		// Releases ticket from the operation.
		void releaseSemaphore();
		// Returns amount of free tickets.
		int getTickets() const;

		//
		// Private members.
		//
	private:
		// This is current number of operations which can be ran.
		int m_tickets = 0;
		// Initial count of tickets. This is done mostly to double check, 
		// that we are not setuping number of tickets when at least on of 
		// the operations which use this semaphore are currently running.
		int m_maxTickets = 1; 
	};
} // namespace bwn