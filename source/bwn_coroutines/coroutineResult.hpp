#pragma once

#include "bwn_core/results/systemResult.hpp"

#include <core/object/ref_counted.h>

#if defined(DEBUG_ENABLED)
	#include "bwn_coroutines/coroutineCreatorInfo.hpp"
	#include <memory>
#endif

namespace bwn
{
	class CoroutineResult : public RefCounted
	{	
		GDCLASS(CoroutineResult, RefCounted);

		//
		// Godot methods.
		//
	protected:
		// Binds methods for the class.
		static void _bind_methods();

		//
		// Public construction and destruction.
		//
	public:
		// By default result is initialised as Success
		CoroutineResult();
		// Default constructor.
		CoroutineResult(const SystemResult& _result);
		// Destructor.
		virtual ~CoroutineResult() override;

		//
		// Public interface.
		//
	public:
		// Shortcut for the result code == k_ok
		bool isSucceeded() const;
		// Shortcut for the result code == k_failed
		bool isFailed() const;
		// Returns result code.
		const SystemResult& getSystemResult() const;

		static Ref<CoroutineResult> createResult(const SystemResult& _result);
		// Creates result and sets SystemREsult from generic SystemFacility.
		static Ref<CoroutineResult> createResult(Error _error, const String& _description);
		static Ref<CoroutineResult> createSuccessResult();

	#if defined(DEBUG_ENABLED)
		void setOperationCreatorInfo(std::shared_ptr<debug::CoroutineCreatorInfo> _info);
		std::shared_ptr<const debug::CoroutineCreatorInfo> getOperationCreatorInfo() const;
	#endif

		//
		// Protected members.
		//
	protected:
		void setSystemResult(const SystemResult& _result);

		//
		// Private methods.
		//
	private:
		// Those methods used basically only for godot initialization.
		void initSystemResultSuccess();
		void initGenericSystemResultError(Error _godotError, const String& _description);
		void initCompleteSystemResultError(SystemResult::NativeError _errorCode, const String& _description, int32_t _facilityId);
		// Those methods are also used only for godot callbacks
		Error getSystemResultCode() const;
		SystemResult::NativeError getSystemResultNativeCode() const;
		String getSystemResultDescription() const;

		//
		// Private members.
		//
	private:
		SystemResult m_result;
	#if defined(DEBUG_ENABLED)
		// Operation creator info. Only used in debug.
		std::shared_ptr<debug::CoroutineCreatorInfo> m_creatorInfo;
	#endif
};

}