#include "precompiled_coroutines.hpp"

#include "bwn_coroutines/coroutineChannel.hpp"
#include "bwn_coroutines/coroutineOperation.hpp"
#include "bwn_coroutines/coroutineContext.hpp"
#include "bwn_coroutines/coroutineManager.hpp"
#include "bwn_coroutines/coroutineSemaphore.hpp"
#include "bwn_coroutines/operations/coroutineWaiterOperation.hpp"

#if defined(DEBUG_ENABLED)

class bwn::CoroutineChannel::DebugCreatorInfo : public debug::CoroutineCreatorInfo
{
	//
	// Creation and destruction.
	//
public:
	DebugCreatorInfo(CoroutineChannel::ChannelId _channelId)
		: debug::CoroutineCreatorInfo(String())
		, m_id( _channelId )
	{}
	virtual ~DebugCreatorInfo() override = default;

	//
	// Public interface.
	//
public:
	virtual String composeDescriptionV() const override
	{
		return bwn::format("Coroutine Channel by Id {}.", m_id);
	}

	//
	// Private member.
	//
private:
	CoroutineChannel::ChannelId m_id;
};

#endif

bwn::CoroutineChannel::CoroutineChannel()
	: m_semaphore()
	, m_waiterOperation()
	, m_id( generateNewId() )
#if defined(DEBUG_ENABLED)
	, m_creatorInfo( std::make_shared<DebugCreatorInfo>(m_id) )
#endif
{}

bwn::CoroutineChannel::~CoroutineChannel()
{
	// In theory, there should not be any errors, 
	// if we will have any operations still running after channel was deleted.
	// Because of that, aborting operations should be decided on channel user side. 
	// abortOperations();
}

bwn::CoroutineChannel::CoroutineChannel(const CoroutineChannel& _other)
	: m_semaphore( _other.m_semaphore )
	, m_waiterOperation() // recording process can't be transfered.
	, m_id( _other.m_id )
#if defined(DEBUG_ENABLED)
	, m_creatorInfo( _other.m_creatorInfo )
#endif
{}
bwn::CoroutineChannel& bwn::CoroutineChannel::operator=(const CoroutineChannel& _other)
{
	if (&_other != this)
	{
		m_id = _other.m_id;
		m_semaphore = _other.m_semaphore;
	}

	return *this;
}

bool bwn::CoroutineChannel::addOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineContext>& _context) const
{
	BWN_ASSERT_WARNING_COND(_operation.is_valid(), "Can't push nullptr operation.");
	if (_operation.is_null()) {
		return false;
	}

	if (m_semaphore.is_valid() && _operation->getSemaphore().is_null())
	{
		// If we have semaphore, but operation doesn't - set default semaphore.
		Ref<CoroutineOperation> operation = _operation;
		operation->setSemaphore(m_semaphore);
	}

	if (m_waiterOperation.is_valid())
	{
		m_waiterOperation->addTrackOperation(_operation);
	}

#if defined(DEBUG_ENABLED)
	Ref<CoroutineOperation> mutableOp = _operation;
	mutableOp->setParentCreatorInfo(m_creatorInfo);
#endif

	return CoroutineManager::getInstance()->addOperation(_operation, _context, m_id);
}

bool bwn::CoroutineChannel::addOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineOperation>& _relativeOperation,
	ECoroutineRelation _relationMode, 
	const Ref<CoroutineContext>& _context) const
{
	BWN_ASSERT_WARNING_COND(_operation.is_valid(), "Can't push nullptr operation.");
	if (_operation.is_null()) {
		return false;
	}

	if (m_semaphore.is_valid() && _operation->getSemaphore().is_null())
	{
		// If we have semaphore, but operation doesn't - set default semaphore.
		Ref<CoroutineOperation> operation = _operation;
		operation->setSemaphore(m_semaphore);
	}

	if (m_waiterOperation.is_valid())
	{
		m_waiterOperation->addTrackOperation(_operation);
	}

#if defined(DEBUG_ENABLED)
	Ref<CoroutineOperation> mutableOp = _operation;
	mutableOp->setParentCreatorInfo(m_creatorInfo);
#endif

	return CoroutineManager::getInstance()->addOperation(_operation, _relativeOperation, _relationMode, _context, m_id);
}

void bwn::CoroutineChannel::startRecording()
{
	cancelRecording();

	m_waiterOperation.instantiate();

#if defined(DEBUG_ENABLED)
	m_waiterOperation->setParentCreatorInfo(m_creatorInfo);
#endif
}

void bwn::CoroutineChannel::finishRecording(const Ref<CoroutineContext>& _context)
{
	BWN_ASSERT_WARNING_COND(m_waiterOperation.is_valid(), "FinishRecording was called on channel which wasn't recording. Are you sure this is correct behavior?");
	if (!m_waiterOperation.is_valid())
	{
		m_waiterOperation.instantiate();
	}

	CoroutineManager::getInstance()->addOperation(m_waiterOperation, _context, m_id);

	cancelRecording();
}

void bwn::CoroutineChannel::cancelRecording()
{
	m_waiterOperation.unref();
}

void bwn::CoroutineChannel::abortOperations() const
{
	CoroutineManager::getInstance()->abortOperations(m_id);
}

void bwn::CoroutineChannel::setSemaphore(const Ref<CoroutineSemaphore>& _semaphore)
{
	m_semaphore = _semaphore;
}

uint32_t bwn::CoroutineChannel::generateNewId()
{
	static uint32_t s_nextId = 0;
	
	const uint32_t ret = s_nextId++;
	
	return ret;
}

void bwn::CoroutineChannel::trackCurrentOperations(const Ref<CoroutineContext>& _context) const
{
	if (_context.is_null())
	{
		return;
	}
	
	std::vector<Ref<CoroutineOperation>> operations = CoroutineManager::getInstance()->getOperations(m_id);
	// Do not track any finished operations.
	operations.erase(
		std::remove_if(
			operations.begin(), 
			operations.end(),
			[](const Ref<CoroutineOperation>& _op) -> bool
			{
				return _op.is_null() || _op->isFinishedRunning();
			}),
		operations.end());
	
	
	Ref<CoroutineWaiterOperation> waiter = bwn::instantiateRef<CoroutineWaiterOperation>(std::move(operations));

#if defined(DEBUG_ENABLED)
	waiter->setParentCreatorInfo(m_creatorInfo);
#endif
	// If there were no unfinished operations left, we still want to create an track operation, 
	// just to report, because no operation is still counts as all operations completed.
	addOperation(waiter, _context);
}

#if defined(DEBUG_ENABLED)
void bwn::CoroutineChannel::setParentCreatorInfo(std::shared_ptr<debug::CoroutineCreatorInfo> _info)
{
	m_creatorInfo->setParentCreator(_info);
}
std::shared_ptr<const bwn::debug::CoroutineCreatorInfo> bwn::CoroutineChannel::getCreatorInfo() const
{
	return m_creatorInfo;
}
#endif

bwn::CoroutineChannelObject::CoroutineChannelObject() = default;

bwn::CoroutineChannelObject::CoroutineChannelObject(const CoroutineChannel& _other)
	: CoroutineChannel(_other)
{}

bwn::CoroutineChannelObject::~CoroutineChannelObject() = default;

void bwn::CoroutineChannelObject::_bind_methods()
{
	// Godot public interface.
	ClassDB::bind_method(D_METHOD("add_operation", "operation", "context"), &CoroutineChannelObject::addOperation);
	ClassDB::bind_method(D_METHOD("add_child_operation", "operation", "parent", "context"), &CoroutineChannelObject::addChildOperation);
	ClassDB::bind_method(D_METHOD("add_and_wait_operation", "operation", "waiter", "context"), &CoroutineChannelObject::addAndWaitOperation);
	ClassDB::bind_method(D_METHOD("add_and_wait_child_operation", "operation", "parent", "context"), &CoroutineChannelObject::addAndWaitChildOperation);
	ClassDB::bind_method(D_METHOD("abort_operations"), &CoroutineChannelObject::abortOperations);
	ClassDB::bind_method(D_METHOD("set_semaphore", "semaphore"), &CoroutineChannelObject::setSemaphore);
	ClassDB::bind_method(D_METHOD("track_current_operations", "context"), &CoroutineChannelObject::trackCurrentOperations);
}

bool bwn::CoroutineChannelObject::addOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineContext>& _context) const
{
	return CoroutineChannel::addOperation(_operation, _context);
}
	
bool bwn::CoroutineChannelObject::addChildOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineOperation>& _parent, 
	const Ref<CoroutineContext>& _context) const
{
	return CoroutineChannel::addOperation(_operation, _parent, ECoroutineRelation::k_parent, _context);
}
	
bool bwn::CoroutineChannelObject::addAndWaitOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineOperation>& _waiterOperation, 
	const Ref<CoroutineContext>& _context) const
{
	return CoroutineChannel::addOperation(_operation, _waiterOperation, ECoroutineRelation::k_waiter, _context);
}
	
bool bwn::CoroutineChannelObject::addAndWaitChildOperation(
	const Ref<CoroutineOperation>& _operation, 
	const Ref<CoroutineOperation>& _parent, 
	const Ref<CoroutineContext>& _context) const
{
	return CoroutineChannel::addOperation(_operation, _parent, ECoroutineRelation(ECoroutineRelation::k_parent | ECoroutineRelation::k_waiter), _context);
}
	
void bwn::CoroutineChannelObject::abortOperations() const
{
	CoroutineChannel::abortOperations();
}

void bwn::CoroutineChannelObject::setSemaphore(const Ref<CoroutineSemaphore>& _semaphore)
{
	CoroutineChannel::setSemaphore(_semaphore);
}

void bwn::CoroutineChannelObject::trackCurrentOperations(const Ref<CoroutineContext>& _context) const
{
	CoroutineChannel::trackCurrentOperations(_context);
}